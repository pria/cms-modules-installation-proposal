<?php

namespace CMS\CoreBundle\Modules\Installation;

class RecorderItem
{
    public $system;
    public $method;
    public $arguments = [];

    public function __construct($system, $methodName, array $arguments)
    {
        $this->system = $system;
        $this->method = $methodName;
        $this->arguments = $arguments;
    }
}
