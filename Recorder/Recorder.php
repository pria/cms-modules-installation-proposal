<?php

namespace CMS\CoreBundle\Modules\Installation;

use Doctrine\ODM\MongoDB\DocumentManager;

class Recorder
{
    public $fileSystem;
    public $doctrine;

    private $info;

    public function __construct(RecorderFileSystem $fs, DocumentManager $dm)
    {
        $this->fileSystem = $fs;
        $this->doctrine = $dm;
    }

    public function start($phaseName)
    {
    }

    public function end()
    {
    }

    public function commit()
    {
    }

    public function rollbackAll()
    {
    }

    public function rollback($phaseName)
    {
    }

    public function setInfo(RecorderInfo $info)
    {
        $this->info = $info;
    }
}
