<?php

namespace CMS\CoreBundle\Modules\Installation;

class RecorderMethodMappingPair
{
    const ONE_TO_ONE_SAME_ORDER = "$0 -> $0";

    public $firstMethod;
    public $secondMethod;

    /**
    Describe how arguments are mapped between two method calls with special syntax.
    Examples:
    $0 -> $0 methods have only one arguments and order is same
    $0, $1 -> $1, $0 methods have two arguments but order is swapped
    */
    public $argumentMapping;

    public function __construct($first, $second, $argumentsMapping = "$0 -> $0")
    {
        $this->firstMethod = $first;
        $this->secondMethod = $second;

        $this->argumentMapping = $argumentsMapping;
    }
}
