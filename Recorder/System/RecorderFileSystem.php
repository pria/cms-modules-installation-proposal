<?php

namespace CMS\CoreBundle\Modules\Installation;

use Symfony\Component\Filesystem\Filesystem;

/**
Proxy class for Symfony Filesystem class
*/
class RecorderFileSystem extends Filesystem implements IRecorderSystem
{
    private $recorderStack;

    public function mkdir($files, $mode = 511)
    {
        // Add item to stack
        $this->recorderStack->push(new RecorderItem(__CLASS__, __METHOD__, [
            $files, $mode
        ]));

        // Call original method
        return parent::mkdir($files, $mode);
    }

    public function remove($files)
    {
        $this->recorderStack->push(new RecorderItem(__CLASS__, __METHOD__, [
            $files
        ]));

        return parent::remove($files);
    }

    public function setRecorderStack(RecorderStack $stack)
    {
        $this->recorderStack = $stack;
    }

    public function getMethodMapping()
    {
        // pairs of methods
        return [
            new RecorderMethodMappingPair("mkdir", "remove", RecorderMethodMappingPair::ONE_TO_ONE_SAME_ORDER)
        ];
    }
}
