<?php

namespace CMS\CoreBundle\Modules\Installation;

interface IRecorderSystem
{
    public function getMethodMapping();
    public function setRecorderStack(RecorderStack $stack);
}
