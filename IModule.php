<?php

namespace CMS\CoreBundle\Modules;

interface IModule
{
    public function getVersion();
    public function getName();
    public function getVersionCheckerEndpoint();

    public function onInstall();
    public function onUninstall();
}
