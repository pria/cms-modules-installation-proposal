# CMS Modules Proposal

Modules are core feature of CMS, but current installation and uninstallation process could be painful.

This proposal adds **IModule** interface which needs to implements every **Module** class in every module bundle.

If module don't contains **Module** class it is invalid and it will not be installed and listed in modules list.

File **IModule.php** contains proposed interface for modules and **Module.php** shows example implementation of interface.

## Classes

Modules system introduces new classes and interfaces:

 - CMS\CoreBundle\Modules\Installation\Recorder
 - CMS\CoreBundle\Modules\Installation\RecorderItem
 - CMS\CoreBundle\Modules\Installation\RecorderMethodMappingPair
 - CMS\CoreBundle\Modules\Installation\RecorderStack
 - CMS\CoreBundle\Modules\Installation\IRecorderSystem
 - CMS\CoreBundle\Modules\Installation\RecorderFileSystem

# Missing features in this proposal

 - Create **Modules\Installation\Manager** which will install or uninstall modules on composer events
 - Add composer events hooks
 - ~~Create Recorder draft implementation~~

# Modules Installation Recorder

Also this proposal adds installation recorder which helps to create collections, files and more.
But crucial feature this recorder is that every installation steps is recorder (and also divided into named steps) into database and on uninstallation recorder could undo steps performed at installation.

## Classes

Recorder introduces two classes:

 - Modules\Installation\Recorder
 - Modules\Installation\RecorderInfo

Recorder behaves like state machine and RecorderInfo is struct with module informations.

## Example

```
// Get recorder from container
$this->recorder = $container->get("module_installation_recorder");

// Create info struct
$this->recorderInfo = new Modules\Installation\RecorderInfo();
$this->recorderInfo->namespace = $this->getName();
$this->recorderInfo->version = $this->getVersion();

// Set info struct
$this->recorder->setInfo($this->recorderInfo);

// Create phase named 'database'
$this->recorder->start("database");

// Create file
$this->recorder->fileSystem->mkdir('dir/' . mt_rand());
// Create collection
$this->recorder->doctrine->createCollection(new TestEntity());

// End phase 'database'
$this->recorder->end();

// Commit all changes
$this->recorder->commit();
```
