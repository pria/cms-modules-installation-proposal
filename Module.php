<?php

namespace PRIA\CMS\ArticlesBundle;

final class Module implements PRIA\CMS\Modules\IModule
{
    /** @var Modules\Installation\Recorder **/
    private $recorder;

    /** @var Modules\Installation\RecorderInfo **/
    private $recorderInfo;

    public function __construct($container)
    {
        $this->recorder = $container->get("module_installation_recorder");

        $this->recorderInfo = new Modules\Installation\RecorderInfo();
        $this->recorderInfo->namespace = $this->getName();
        $this->recorderInfo->version = $this->getVersion();
    }

    public function onInstall()
    {
        $this->recorder->setInfo($this->recorderInfo);

        // Create phase
        $this->recorder->start("database");

        $this->recorder->fileSystem->mkdir('dir/' . mt_rand()); // Create file
        $this->recorder->doctrine->createCollection(new TestEntity()); // Create collection

        // End phase
        $this->recorder->end();

        // Commit all changes
        $this->recorder->commit();
    }

    public function onUninstall()
    {
        $this->recorder->setInfo($this->recorderInfo);

        // Rollback all
        $this->recorder->rollbackAll();

        // OR rollback only selected phase
        $this->recorder->rollback("database");
    }

    public function getVersion()
    {
        return "1.0.0";
    }

    public function getName()
    {
        return "Articles";
    }

    public function getVersionCheckerEndpoint()
    {
        return "http://cms.pria.cz/modules/versions/articles_bundle/stable";
    }
}
